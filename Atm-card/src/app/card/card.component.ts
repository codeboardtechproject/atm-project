import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent {
popup=false;
constructor(private http: HttpClient) {}
  OnCreate(postdata: { Withdrawal: string }) {
    this.http
      .post(
        'https://angular-21e06-default-rtdb.firebaseio.com/data.json',
        postdata
      )
      .subscribe((Response) => {
        console.table(Response);
      });
  }

}
