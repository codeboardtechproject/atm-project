import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CardComponent } from './card/card.component';
import { LoginComponent } from './login/login.component';
import { CardDetailComponent } from './card-detail/card-detail.component';
import { TransactionComponent } from './transaction/transaction.component';
import { ChartComponent } from './chart/chart.component';
const routes: Routes = [
  {path:"",redirectTo:"login",pathMatch:"full"},
  {path:'card',component:CardComponent},
  {path:'card-detail',component:CardDetailComponent},
  {path:'login',component:LoginComponent},
  {path:'transaction',component:TransactionComponent},
  {path:'chart',component:ChartComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
