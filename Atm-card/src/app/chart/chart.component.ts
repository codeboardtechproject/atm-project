import { Component } from '@angular/core';
import { Chart } from 'angular-highcharts';
@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent {
  lineChart=new Chart({
    chart:{
      type:'line'
    },
    title:{
      text:'Withdrawal-Amount'
    },
    credits:{
      enabled:false
    },
    series:[
      {
        name:"Time",
        data:[1,2,3,1,5,6,3,8,4]
      }as any
    ]
   })
  
}
