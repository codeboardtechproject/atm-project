import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {MatCardModule} from '@angular/material/card';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
 import {MatFormFieldModule} from '@angular/material/form-field';
 import {MatSelectModule} from '@angular/material/select';
 import { FormsModule } from '@angular/forms';
 import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { LoginComponent } from './login/login.component';
import { CardComponent } from './card/card.component';
import { CardDetailComponent } from './card-detail/card-detail.component';
import { HttpClientModule } from '@angular/common/http';
import { ApiService } from './api.service';
import{MatDialogModule}from '@angular/material/dialog';
import { TransactionComponent } from './transaction/transaction.component';
import { ChartModule } from 'angular-highcharts';
import { ChartComponent } from './chart/chart.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    CardComponent,
    CardDetailComponent,
    TransactionComponent,
    ChartComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,FormsModule,
    MatSelectModule,
    MatInputModule,
    MatCardModule,
    BrowserAnimationsModule,
     MatFormFieldModule,
     MatButtonModule,
     HttpClientModule,
     MatDialogModule,
     ChartModule,
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
