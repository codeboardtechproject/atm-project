import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../api.service';
@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.css']
})
export class TransactionComponent {
  todo: any;
  constructor(private http: HttpClient) {}
  Event() {
    this.http
      .get('https://angular-21e06-default-rtdb.firebaseio.com/data.json')
      .subscribe((response: any) => {
        this.todo = response;
      });
  }
}


