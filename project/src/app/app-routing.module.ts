import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CardComponent } from './card/card.component';
import { LoginComponent } from './login/login.component';
import { GenerateComponent } from './generate/generate.component';
import { HistoryComponent } from './history/history.component';
const routes: Routes = [
  {path:"",redirectTo:"login",pathMatch:"full"},
  {path:'card',component:CardComponent},
  {path:'generate',component:GenerateComponent},
  {path:'login',component:LoginComponent},
  {path:'history',component:HistoryComponent}
  

];

@NgModule({
  imports:[RouterModule.forRoot(routes)],
  exports:[RouterModule]
})
export class AppRoutingModule { }