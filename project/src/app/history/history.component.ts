import { Component,OnInit } from '@angular/core';
import{HttpClient} from '@angular/common/http';


@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
todo:any;
Withdrawal:any;
Balance:any;
  constructor(private http:HttpClient){}
  ngOnInit(){
    this.http.get('https://angular-21e06-default-rtdb.firebaseio.com/Bank.json')
    .subscribe((Responce) =>  {
      this.todo = Responce;
    });

    this.http.get('https://angular-21e06-default-rtdb.firebaseio.com/Withdraw.json')
    .subscribe((Responce) =>  {
      this.Withdrawal = Responce;
    });

    this.http.get('https://angular-21e06-default-rtdb.firebaseio.com/balance.json')
    .subscribe((Responce) =>  {
      this.Balance = Responce;
    });
  }

}
